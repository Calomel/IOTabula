# IOTabula, a table input/output library

## Building and Installation

To build the library (Note: for testing, see the section below):

```
mkdir build && cd build
cmake ../src -DCMAKE_BUILD_TYPE=Release
make
```

## Testing

To build the library with tests:

```
cmake ../src -DCMAKE_BUILD_TYPE=Debug -DTEST=1
```

To run tests

```
ctest [--output-on-failure]
```
