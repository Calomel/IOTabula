#include <sstream>

#include <common/Suite.hpp>
#include <IOTabula/OCSVStream.hpp>

using namespace iotab;

void case_1(Suite& suite)
{
	std::ostringstream os;
	OCSVStream ocsv = OCSVStream::writeStream(&os);

	Row r; r.resize(4);

	r[0] = "aa";
	r[1] = "bb";
	r[2] = "cc";
	r[3] = "dd dd";

	ocsv << r;

	r[0] = "a,b";
	r[1] = "c,d";
	r[2] = "\"\"";
	r[3] = "\",\"";

	ocsv << r;

	suite.assertEqual(os.str(),
		"aa,bb,cc,dd dd\n"
		"\"a,b\",\"c,d\",\"\"\"\",\"\"\",\"\"\"\n");
}
void case_2(Suite& suite)
{
	std::ostringstream os;
	OCSVStream ocsv = OCSVStream::writeStream(&os);

	Row r; r.resize(4);

	r[0] = "\n";
	r[1] = ",";
	r[2] = "\"";
	r[3] = "\n,\n";

	ocsv << r;
	suite.assertEqual(os.str(),
		"\"\n\",\",\",\"\",\"\n,\n\"\n");
}

int main(int argc, char* argv[])
{
	Suite suite;

	case_1(suite);
	case_2(suite);

	return suite.accumulate();
}
