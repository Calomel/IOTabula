#include <sstream>

#include <common/Suite.hpp>
#include <IOTabula/ICSVStream.hpp>

using namespace iotab;

void case_1(Suite& suite)
{
	std::cout << "case_1\n";
	std::istringstream is("a,b,c,1,2,3\n4,5,6,7,8,9\n");

	ICSVStream icsv = ICSVStream::readStream(&is);

	Row r;

	icsv >> r;

	bool flag = r.length() == 6;
	suite.assertEqual(r.length(), 6, "Length");

	if (flag)
	{
		suite.assertEqual(r[0], "a", "0");
		suite.assertEqual(r[1], "b", "1");
		suite.assertEqual(r[2], "c", "2");
		suite.assertEqual(r[3], "1", "3");
		suite.assertEqual(r[4], "2", "4");
		suite.assertEqual(r[5], "3", "5");
	}

	suite.assert(!icsv.isEof(), "!Eof");

	icsv >> r;

	flag = r.length() == 6;
	suite.assertEqual(r.length(), 6, "Length");
	if (flag)
	{
		suite.assertEqual(r[0], "4", "0");
		suite.assertEqual(r[1], "5", "1");
		suite.assertEqual(r[2], "6", "2");
		suite.assertEqual(r[3], "7", "3");
		suite.assertEqual(r[4], "8", "4");
		suite.assertEqual(r[5], "9", "5");
	}
	suite.assert(icsv.isEof(), "Eof");

	
	if (!icsv.isEof())
	{
		std::cerr << "Stream contains unread contents:\n";
		icsv >> r;
		for (int i = 0; i < r.length(); ++i)
		{
			std::cout << "\tExtra: ";
			for (auto const c: r[i])
			{
				std::cout << "<" << (int) c << ">";
			}
			std::cout << '\n';
		}
		suite.assert(icsv.isEof(), "Eof2");
	}
}

void case_2(Suite& suite)
{
	std::cout << "case_2\n";
	// "aabb","aa,bb",aa bb,"",aa,bb,a""b
	// ^      ^       ^     ^  ^  ^  ^
	std::istringstream is("\"aabb\",\"aa,bb\",aa bb,\"\",aa,bb,a\"\"b\n");
	ICSVStream icsv = ICSVStream::readStream(&is);

	Row r;
	icsv >> r;

	bool flag = r.length() == 7;
	suite.assertEqual(r.length(), 7, "Length");

	if (flag)
	{
		suite.assertEqual(r[0], "aabb", "0");
		suite.assertEqual(r[1], "aa,bb", "1");
		suite.assertEqual(r[2], "aa bb", "2");
		suite.assertEqual(r[3], "\"", "4");
		suite.assertEqual(r[4], "aa", "5");
		suite.assertEqual(r[5], "bb", "6");
		suite.assertEqual(r[6], "a\"b", "7");
	}
	else
	{
		std::cout << "Content: " << r.toString() << '\n';
	}
	suite.assert(icsv.isEof(), "Eof");
}
void case_3(Suite& suite)
{
	std::cout << "case_3\n";
	std::istringstream is("aa,bb\ncc,00-11,\"abc,def,\"\n");
	ICSVStream icsv = ICSVStream::readStream(&is);

	Row r;
	icsv >> r;
	suite.assertEqual(r.length(), 2, "Length 1");
	icsv >> r;
	suite.assertEqual(r.length(), 3, "Length 2");
	if (r.length() != 3)
		std::cout << "Content: " << r.toString() << '\n';

	suite.assert(icsv.isEof(), "Eof");
}

int main(int argc, char* argv[])
{
	Suite suite;

	case_1(suite);
	case_2(suite);
	case_3(suite);

	return suite.accumulate();
}
