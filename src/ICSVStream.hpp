/**
 * This file is a part of the IO Tabula library
 */

#ifndef _IOTABULA_ICSVSTREAM_HPP
#define _IOTABULA_ICSVSTREAM_HPP

#include <istream>

#include "Row.hpp"

namespace iotab
{

/**
 * Reads Rows from a string stream or file in CSV format
 */
class ICSVStream final
{
public:
	ICSVStream()
		: in_(nullptr), buffer_()
	{}
	~ICSVStream();

	static ICSVStream readFile(char const* filename);
	static ICSVStream readStream(std::istream*);

	ICSVStream& operator>>(Row&);

	void seekBegin();
	bool isEof() const;
private:
	ICSVStream(std::istream*, bool ownership);

	bool streamOwnership_;
	std::istream* in_;
	std::string buffer_;
};


} // namespace iotab

#endif // !_IOTABULA_ICSVSTREAM_HPP
