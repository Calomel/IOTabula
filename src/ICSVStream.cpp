#include "ICSVStream.hpp"

#include <cassert>
#include <fstream>

namespace iotab
{

ICSVStream::ICSVStream(std::istream* s, bool ownership)
	: streamOwnership_(ownership)
	, in_(s)
{
	assert(s);

	constexpr std::size_t Reserve = 64;
	buffer_.reserve(Reserve);
}
ICSVStream::~ICSVStream()
{
	if (streamOwnership_)
		delete in_;
}

ICSVStream
ICSVStream::readFile(char const* filename)
{
	std::ifstream* const stream = new std::ifstream(filename);

	return ICSVStream(stream, true);
}
ICSVStream
ICSVStream::readStream(std::istream* is)
{
	return ICSVStream(is, false);
}

ICSVStream&
ICSVStream::operator>>(Row& r)
{
	r.clear();

	if (isEof())
		return *this;

	buffer_.clear();

	// Main interpreter loop
	int ch;
	while (true)
	{
		ch = in_->get();

next_loop:;
		if (ch == ',')
		{
			// This buffer is complete.
			r.add(buffer_);
			buffer_.clear();
		}
		else if (ch == '"')
		{
			ch = in_->get();
			buffer_ += ch;
			if (ch == '"')
				continue;
			// Read until we encounter the next " that is not immediately followed by
			// a "
			do
			{
				ch = in_->get();
				if (ch == '"')
				{
					int const ch2 = in_->get();

					// If this second ch is also a ", a " is escaped
					if (ch2 == '"')
					{
						buffer_ += '"';
						continue;
					}
					else
					{
						// Bracket closes.
						ch = ch2;
						goto next_loop;
						break;
					}
				}
				else
					buffer_ += ch;
			} while (ch != EOF);
		}
		else if (ch == '\n' || ch == EOF)
		{
			r.add(buffer_);
			buffer_.clear();
			break;
		}
		else
			buffer_ += ch;
	}

	return *this;
}

void
ICSVStream::seekBegin()
{
	in_->seekg(in_->beg);
}

bool
ICSVStream::isEof() const
{
	if (in_->eof())
		return true;
	// If the input stream is
	//
	// 1,2,3\n
	// 4,5,6\n
	//
	// The stream's state will not be EOF after both lines are read. Therefore
	// we peek the next character and consume it if necessary.
	if (in_->peek() == EOF)
	{
		in_->get();
		return true;
	}
	return false;
}

} // namespace iotab
