#include "OCSVStream.hpp"

#include <cassert>
#include <fstream>
#include <stdexcept>

namespace iotab
{

OCSVStream::OCSVStream(std::ostream* s, bool ownership)
	: streamOwnership_(ownership)
	, out_(s)
	, rowBegin_(true)
{
	assert(s);
}
OCSVStream::~OCSVStream()
{
	if (streamOwnership_)
		delete out_;
}

OCSVStream
OCSVStream::writeFile(char const* filename)
{
	std::ofstream* const stream = new std::ofstream(filename);

	return OCSVStream(stream, true);
}
OCSVStream
OCSVStream::writeStream(std::ostream* stream)
{
	return OCSVStream(stream, false);
}

OCSVStream&
OCSVStream::operator<<(Row const& r)
{
	if (!rowBegin_)
		throw std::runtime_error("Cannot print new row at the middle of a row");

	const int n = r.length();
	
	if (n == 0)
	{
		*out_ << '\n';
		return *this;
	}

	*out_ << escape(r[0]);
	for (int i = 1; i < n; ++i)
		*out_ << "," << escape(r[i]);
	*out_ << '\n';

	newline();

	return *this;
}
OCSVStream&
OCSVStream::operator<<(std::string const& str)
{
	if (rowBegin_)
	{
		*out_ << escape(str);
		rowBegin_ = false;
		return *this;
	}
	else
	{
		*out_ << "," << escape(str);
		return *this;
	}
}
OCSVStream&
OCSVStream::newline()
{
	if (rowBegin_)
		return *this;
	*out_ << '\n';
	rowBegin_ = true;
	return *this;
}

std::string
OCSVStream::escape(std::string const& s)
{
	// Pass 1 to determine if there are commas and "
	bool hasComma = false;
	bool hasQuote = false;
	for (auto const c: s)
	{
		if (c == ',' || c == '\n')
			hasComma = true;
		else if (c == '"')
			hasQuote = true;
	}

	if (hasComma)
	{
		if (hasQuote)
		{
			std::string buffer;
			buffer += '"';
			for (auto const c: s)
			{
				if (c == '"')
					buffer += "\"\"";
				else
					buffer += c;
			}
			buffer += '"';
			return buffer;
		}
		else
			return '"' + s + '"';
	}
	else
	{
		if (hasQuote)
		{
			std::string buffer;
			for (auto const c: s)
			{
				if (c == '"')
					buffer += "\"\"";
				else
					buffer += c;
			}
			return buffer;
		}
		else
			return s;
	}
}


} // namespace iotab
