/**
 * This file is a part of the IO Tabula library
 */
#ifndef _IOTABULA_ROW_HPP
#define _IOTABULA_ROW_HPP

#include <string>
#include <vector>

namespace iotab
{

/**
 * Wrapper of an array of string
 */
class Row final
{
public:
	Row() = default;

	std::string const& operator[](int i) const { return data_[i]; }
	std::string&       operator[](int i)       { return data_[i]; }
	std::size_t length() const { return data_.size(); }

	void resize(std::size_t size) { data_.resize(size); }
	void clear() { data_.clear(); }

	void add(std::string const& s) { data_.push_back(s); }

	std::string toString() const;
private:
	std::vector<std::string> data_;
};

} // namespace iotab

#endif // !_IOTABULA_ROW_HPP
